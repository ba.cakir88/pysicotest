import React from 'react';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { SafeAreaProvider } from "react-native-safe-area-view";

import { BottomTabNavigator } from "./components/BottomTabNavigator";

const Stack = createStackNavigator();

export default function App() {
  return (
<SafeAreaProvider>      
        <NavigationContainer>
          <Stack.Navigator initialRouteName={BottomTabNavigator}>
            <Stack.Screen
              component={BottomTabNavigator}
              name="BottomTabNavigator"
              options={{ headerShown: false }}
            />
            <Stack.Screen
              component={SinglePostScreen}
              name="SinglePost"
              options={singlePostNavigationOptions}
            />
            <Stack.Screen
              component={StatusUpdateScreen}
              name="NewPost"
              options={statusUpdateNavigationOptions}
            />
            <Stack.Screen
              component={EditProfileScreen}
              name="EditProfile"
              options={editProfileNavigationOptions}
            />
          </Stack.Navigator>
        </NavigationContainer>
    </SafeAreaProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
